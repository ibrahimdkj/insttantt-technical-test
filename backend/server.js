const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const jwt = require("jsonwebtoken");
const secretKey = "mysecretkey";
const mysql = require("mysql2");
const multer = require("multer");
const mongoose = require("mongoose");
app.use(bodyParser.json());
app.use(cors());
const http = require("http").Server(app);
const io = require("socket.io")(http, {
  cors: {
    origin: "http://localhost:4200",
    methods: ["GET", "POST"],
    allowedHeaders: ["my-custom-header"],
    credentials: true,
  },
});

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "insttantt",
});

mongoose.connect("mongodb://localhost:27017/mydatabase", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;

db.on("error", (error) => console.error(error));
db.once("open", () => console.log("Conexión a la base de datos mongo exitosa"));

connection.connect((err) => {
  if (err) throw err;
  console.log("Conexión a la base de datos mysql exitosa");
});

// Configurar multer para manejar el archivo de imagen
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "uploads/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage });

app.use("/uploads", express.static("uploads"));

// Redirigir al cliente a la URL /inicio
app.get("/", (req, res) => {
  res.send("¡Bienvenido a mi aplicación Express!");
});

/*	-------------------------------------------	*\ 
/*	USUARIOS 
/*	-------------------------------------------	*/

app.post("/usuarios", (req, res) => {
  // Aquí se procesa la información del usuario
  const {
    email,
    phoneNumber,
    firstname,
    lastname,
    documentType,
    documentNumber,
    birthdate,
    expeditionDate,
  } = req.body;

  // Aquí se inserta la información del usuario en la tabla
  const sql =
    "INSERT INTO usuarios (email, phoneNumber, firstname, lastname, documentType, documentNumber, birthdate, expeditionDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
  const values = [
    email,
    phoneNumber,
    firstname,
    lastname,
    documentType,
    documentNumber,
    birthdate,
    expeditionDate,
  ];
  connection.query(sql, values, (error, results, fields) => {
    if (error) throw error;
    console.log("Usuario creado con éxito");
    // res.send("Usuario creado con éxito");
  });

  // Aquí se crea el token para el usuario
  const payload = {
    email,
    firstname,
    lastname,
    documentNumber,
  };
  const token = jwt.sign(payload, secretKey);

  // Aquí se devuelve la respuesta con el token
  res.json({ token });
});

app.put("/usuarios/:id", upload.single("file"), function (req, res) {
  const { id } = req.params;
  const {
    email,
    phoneNumber,
    firstname,
    lastname,
    documentType,
    documentNumber,
    birthdate,
    expeditionDate,
    country,
    city,
    address,
  } = req.body;
  const file = req.file ? req.file.filename : null;
  // Realizar la consulta de actualización a la base de datos
  connection.query(
    "UPDATE usuarios SET email = ?, phoneNumber = ?, firstname = ?, lastname = ?, documentType = ?, documentNumber = ?, birthdate = ?, expeditionDate = ?, country = ?, city = ?, address = ?, photoProﬁle = ? WHERE id = ?",
    [
      email,
      phoneNumber,
      firstname,
      lastname,
      documentType,
      documentNumber,
      birthdate,
      expeditionDate,
      country,
      city,
      address,
      file,
      id,
    ],
    (error, results) => {
      if (error) {
        console.error(error);
        return res
          .status(500)
          .json({ message: "Error al actualizar el usuario" });
      }

      return res.json({ message: "Usuario actualizado correctamente" });
    }
  );
});

app.get("/usuarios/:id", (req, res) => {
  const { id } = req.params;
  // Hacer una consulta SQL para recuperar los elementos de la tabla usuarios
  const sql = "SELECT * FROM usuarios WHERE id = ?";
  connection.query(sql, [id], (error, results, fields) => {
    if (error) throw error;
    // Enviar los resultados al cliente en forma de objeto JSON
    console.log(results[0]);
    res.json(results[0]);
  });
});

app.get("/usuarios", (req, res) => {
  // Hacer una consulta SQL para recuperar los elementos de la tabla usuarios
  const sql = "SELECT * FROM usuarios";
  connection.query(sql, (error, results, fields) => {
    if (error) throw error;
    // Enviar los resultados al cliente en forma de objeto JSON
    res.json(results);
  });
});

app.post("/login", (req, res) => {
  const { email, phoneNumber } = req.body;
  console.log(email);
  // Buscar el usuario en la base de datos por correo electrónico y número de teléfono
  const sql = "SELECT * FROM usuarios WHERE email = ? AND phoneNumber = ?";
  connection.query(sql, [email, phoneNumber], (error, results, fields) => {
    if (error) throw error;

    if (results.length > 0) {
      // Generar un token para el usuario autenticado
      const secretKey = "clave-secreta";
      const payload = { email: email, phoneNumber: phoneNumber };
      const token = jwt.sign(payload, secretKey, { expiresIn: "12" });
      const { username, id } = results[0];
      // Enviar el token al cliente en forma de objeto JSON
      res.json({ token: token, username, id });
    } else {
      res.status(401).json({ error: "Usuario o contraseña incorrectos" });
    }
  });
});

const hobbySchema = new mongoose.Schema({
  name: String,
  user: Number,
});

const Hobby = mongoose.model("Hobby", hobbySchema);

app.use(bodyParser.json());

io.on("connection", async (socket) => {
  const userId = socket.handshake.query.userId;
  console.log("Usuario busqueda:" + userId);
  try {
    const hobbies = await Hobby.find({ user: userId });
    socket.emit("init", hobbies);
  } catch (error) {
    console.error(error);
  }

  // Manejar la petición para agregar un nuevo hobby
  socket.on("addHobby", async (data) => {
    console.log("Nuevo hobby recibido:", data.name);

    const hobby = new Hobby({ name: data.name, user: data.id_usuario });
    await hobby.save();

    io.emit("newHobby", hobby);
  });
});

app.listen(3000, () => {
  console.log("API escuchando en el puerto 3000");
});

http.listen(4000, () => {
  console.log("Server started on port 4000");
});
