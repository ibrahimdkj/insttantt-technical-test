# Insttantt TechTest Senior

## Aspectos generales

El proyecto cuenta con 3 carpetas:

- BD
- Backend
- Frontend

En cada carpeta se encuentra el respectivo Readme.md para realizar la ejecución del proyecto, realizarlo en el orden indicado de las carpetas.

## Tecnologías Utilizadas

### BD

Para las bases de datos se utilizó **MySql** y **MongoDB**.

### Backend

Para el Backend se utilizo **Node.js** con **Express** y para el proyecto se utilizo también un server **Socket.io**

### FrontEnd

Para el FrontEnd se utilizó **Angular** en su versión **14.2**, los estilos fueron implementados a código puro utilizando **SCSS** , se utilizo la librería de **Sweetalert2** para mostrar notificaciones, se implemento el uso de los colores según el tema descrito en el PDF,se utilizo las fuentes **Montserrat** y **Raleway**, se utilizó **fontawesome** para iconos, la imagen utilizado como **logo** fue tomada de: https://icons8.com/icon/YCnIGJKTdIDI/homeadvisor,
