import { Layout, Schemes, Theme } from 'src/app/layout/interfaces/layout.types';

export interface AppConfig {
  layout: Layout;
  scheme: Schemes;
  theme: Theme;
}

export const appConfig: AppConfig = {
  layout: 'default',
  scheme: 'light',
  theme: 'default',
};
