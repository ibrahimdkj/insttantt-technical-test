import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { EXPIRATION_TIME, SESSION_KEY } from '../const/data';
import { Router } from '@angular/router';
import { NotificationService } from '../../../shared/services/notification.service';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly SESSION_KEY = SESSION_KEY;
  private readonly EXPIRATION_TIME = EXPIRATION_TIME;
  private readonly _isLoggedIn = new BehaviorSubject<boolean>(false);
  private _userSession = new BehaviorSubject<string>('');

  constructor(
    private _httpService: HttpClient,
    private readonly _router: Router,
    private _notificationService: NotificationService
  ) {
    this._isLoggedIn.next(this.getSessionData() !== undefined);
    this._userSession.next(this.getSessionData()?.username ?? '');
  }

  public get userSession() {
    return this._userSession.asObservable();
  }

  public get isLoggedIn() {
    return this._isLoggedIn.asObservable();
  }

  public getSessionData() {
    if (sessionStorage.getItem(this.SESSION_KEY)) {
      const sessionElement = sessionStorage.getItem(this.SESSION_KEY);
      const sessionObject = JSON.parse(sessionElement ?? '{}');

      if (sessionElement && sessionObject?.expiration > Date.now()) {
        return sessionObject.data;
      } else {
        this.clearSession();
        return null;
      }
    }
  }

  public setSessionData(sessionData: any) {
    const expirationTimestamp = Date.now() + this.EXPIRATION_TIME;
    const sessionObject = {
      data: sessionData,
      expiration: expirationTimestamp,
    };
    sessionStorage.setItem(this.SESSION_KEY, JSON.stringify(sessionObject));
    this._isLoggedIn.next(true);
  }

  public clearSession() {
    sessionStorage.removeItem(this.SESSION_KEY);
    this._isLoggedIn.next(false);
  }

  public refreshSesion() {
    const data = this.getSessionData();
    if (data) {
      const sessionData = {
        ...data,
      };
      this.setSessionData(sessionData);
    }
  }

  loginService(email: string, phoneNumber: string): Observable<any> {
    const url = `${environment.BASE_URL}/login`;
    const body = { email: email, phoneNumber: phoneNumber };
    return this._httpService.post(url, body).pipe(
      catchError((error) => {
        const errorMessage =
          error?.error?.error || 'Error al hacer la petición';
        this._notificationService.showError(errorMessage);
        return throwError(() => error);
      })
    );
  }

  public logIn(username: string, password: string) {
    this.loginService(username, password).subscribe((response) => {
      const sessionData = {
        ...response,
        username,
      };
      this.setSessionData(sessionData);
      this._userSession.next(username);
      this._notificationService.showSuccess('Bienvenido');
      this._router.navigate(['/dashboard']);
    });
  }

  createUserService(body: any): Observable<any> {
    const url = `${environment.BASE_URL}/usuarios`;
    return this._httpService.post(url, body).pipe(
      catchError((error) => {
        const errorMessage =
          error?.error?.error || 'Error al hacer la petición';
        this._notificationService.showError(errorMessage);
        return throwError(() => error);
      })
    );
  }
  updateUserService(body: any, id: number): Observable<any> {
    const url = `${environment.BASE_URL}/usuarios/${id}`;
    return this._httpService.put(url, body).pipe(
      catchError((error) => {
        const errorMessage =
          error?.error?.error || 'Error al hacer la petición';
        this._notificationService.showError(errorMessage);
        return throwError(() => error);
      })
    );
  }

  public createUser(body: any) {
    this.createUserService(body).subscribe((response) => {
      this.logIn(body.email, body.phoneNumber);
    });
  }

  public logOut() {
    this.clearSession();
    this._router.navigate(['/login']);
  }
}
