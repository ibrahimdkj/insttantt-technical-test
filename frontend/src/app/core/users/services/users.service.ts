import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NotificationService } from '../../../shared/services/notification.service';
import { environment } from 'src/environments/environment';
import { Observable, catchError, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private _httpService: HttpClient,
    private readonly _router: Router,
    private _notificationService: NotificationService
  ) {}

  getUser(id: number): Observable<any> {
    const url = `${environment.BASE_URL}/usuarios/${id}`;
    return this._httpService.get(url).pipe(
      catchError((error) => {
        const errorMessage =
          error?.error?.error || 'Error al hacer la petición';
        this._notificationService.showError(errorMessage);
        return throwError(() => error);
      })
    );
  }
}
