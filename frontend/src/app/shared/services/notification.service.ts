import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';
import { NotificationInterface } from '../interfaces/notification.interface';
import { NotificationType } from '../types/notification.type';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  constructor() {}

  showMessage(message: string, type: NotificationType, timer: number = 0) {
    const options: NotificationInterface = {
      title: message,
      icon: type,
    };

    if (timer > 0) {
      options['timer'] = timer;
    }
    Swal.fire(options);
  }

  showSuccess(message: string, timer: number = 0) {
    this.showMessage(message, 'success', timer);
  }
  showError(message: string, timer: number = 0) {
    this.showMessage(message, 'error', timer);
  }
  showInfo(message: string, timer: number = 0) {
    this.showMessage(message, 'info', timer);
  }
}
