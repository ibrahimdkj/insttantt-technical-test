import { CityInterface } from '../interfaces/city.interface';

export const citesList: CityInterface[] = [
  {
    id: 1,
    id_country: 1,
    value: '1',
    label: 'Bucaramanga',
  },
  {
    id: 2,
    id_country: 1,
    value: '2',
    label: 'Bogota D.C',
  },
  {
    id: 3,
    id_country: 1,
    value: '3',
    label: 'Cali',
  },
  {
    id: 4,
    id_country: 2,
    value: '4',
    label: 'Madrid',
  },
  {
    id: 5,
    id_country: 2,
    value: '5',
    label: 'Barcelona',
  },
  {
    id: 6,
    id_country: 3,
    value: '6',
    label: 'New York',
  },
  {
    id: 7,
    id_country: 3,
    value: '7',
    label: 'Miami',
  },
];
