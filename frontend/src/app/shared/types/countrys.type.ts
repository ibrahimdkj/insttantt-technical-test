import { CountryInterface } from '../interfaces/country.interface';
export const countrysList: CountryInterface[] = [
  {
    id: 1,
    value: '1',
    label: 'Colombia',
    codigo: 'CO',
  },
  {
    id: 2,
    value: '2',
    label: 'España',
    codigo: 'ES',
  },
  {
    id: 3,
    value: '3',
    label: 'Estados Unidos',
    codigo: 'US',
  },
];
