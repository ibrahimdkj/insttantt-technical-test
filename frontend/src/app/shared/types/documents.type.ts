import { SelectOptionInterface } from '../interfaces/select.interface';

export const documentList: SelectOptionInterface[] = [
  {
    value: 'CC',
    label: 'Cédula de ciudadanía',
  },
  {
    value: 'CE',
    label: 'Cédula de expedición',
  },
  {
    value: 'TI',
    label: 'Tarjeta de identidad',
  },
  {
    value: 'PA',
    label: 'Pasaporte',
  },
];
