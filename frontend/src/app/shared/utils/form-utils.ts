import { FormControl } from '@angular/forms';

export function errorText(control: FormControl) {
  if (control?.hasError('email')) {
    return 'Error de email invalido';
  }

  if (control?.hasError('required')) {
    return 'Este campo es requerido';
  }
  if (control?.hasError('fechaMayorError')) {
    return 'La fecha digitada es Mayor a la permitida';
  }
  if (control?.hasError('fechaMenorError')) {
    return 'La fecha digitada es Menor a la permitida';
  }

  if (control?.hasError('maxlength')) {
    return (
      'Solo puede ser máximo ' +
      control?.getError('maxlength').requiredLength +
      ' caracteres'
    );
  }

  if (control?.hasError('minlength')) {
    return (
      'Debe tener al menos ' +
      control?.getError('minlength').requiredLength +
      ' caracteres'
    );
  }

  if (control?.hasError('mayorEdadError')) {
    return 'Error la fecha registrada indica que tienes menos de 18 años';
  }

  return 'Error';
}

export function hasErrorInput(control: FormControl) {
  const ref = control;
  return ref?.invalid && (ref?.dirty || ref?.touched);
}
