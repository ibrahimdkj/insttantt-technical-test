
export function compararFechasSinHoras(fecha1: Date, fecha2: Date) {
  fecha1.setHours(0, 0, 0, 0);
  fecha2.setHours(0, 0, 0, 0);

  if (fecha1 < fecha2) {
    return -1;
  } else if (fecha1 > fecha2) {
    return 1;
  } else {
    return 0;
  }
}

export function agregarAniosFecha(fecha: Date, anios: number) {
  const fechaNueva = new Date(fecha);
  fechaNueva.setFullYear(fechaNueva.getFullYear() + anios);
  return fechaNueva;
}
