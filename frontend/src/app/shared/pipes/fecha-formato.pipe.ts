import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fechaFormato',
})
export class FechaFormatoPipe implements PipeTransform {
  transform(fecha: Date, formato: string): string {
    const datePipe = new DatePipe('en-US');
    return datePipe.transform(fecha, formato) ?? '';
  }
}
