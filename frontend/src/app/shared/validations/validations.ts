import { AbstractControl, FormControl, ValidatorFn } from '@angular/forms';
import { agregarAniosFecha, compararFechasSinHoras } from '../utils/date-utils';

export function validacionMaxFecha(fechaActual: Date): ValidatorFn {
  return (control: AbstractControl<any, any>) => {
    if (!(control instanceof FormControl)) {
      return null;
    }

    const fechaComparar = new Date(control.value + 'T00:00:00-05:00');

    if (compararFechasSinHoras(fechaActual, fechaComparar) < 0)
      return { fechaMayorError: true };

    return null;
  };
}
export function validacionMinFecha(fechaActual: Date): ValidatorFn {
  return (control: AbstractControl<any, any>) => {
    if (!(control instanceof FormControl)) {
      return null;
    }

    const fechaComparar = new Date(control.value + 'T00:00:00-05:00');

    if (compararFechasSinHoras(fechaActual, fechaComparar) > 0)
      return { fechaMenorError: true };

    return null;
  };
}
export function validacionMayorDeEdad(): ValidatorFn {
  const fechaActual: Date = agregarAniosFecha(new Date(), -18);
  return (control: AbstractControl<any, any>) => {
    if (!(control instanceof FormControl)) {
      return null;
    }

    const fechaComparar = new Date(control.value + 'T00:00:00-05:00');

    if (compararFechasSinHoras(fechaActual, fechaComparar) < 0)
      return { mayorEdadError: true };

    return null;
  };
}

export default {
  validacionMaxFecha,
  validacionMinFecha,
  validacionMayorDeEdad,
};
