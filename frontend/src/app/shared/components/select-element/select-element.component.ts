import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { errorText, hasErrorInput } from '../../utils/form-utils';

@Component({
  selector: 'app-select-element',
  templateUrl: './select-element.component.html',
})
export class SelectElementComponent implements OnInit {
  @Input() id?: string = '';
  @Input() label?: string;
  @Input() name?: string;
  @Input() options: { value: string; label: string }[] = [];
  @Input() placeholder = '';
  @Input() control: any;
  @Input() required = false;
  @Input() visible = true;
  @Input() disabled = false;

  @Output() changeEvent: EventEmitter<any> = new EventEmitter();

  errorText = errorText;
  hasErrorInput = hasErrorInput;

  constructor() {}

  ngOnInit(): void {}

  valueChanged(event: any) {
    this.changeEvent?.emit(event?.target?.value);
  }
}
