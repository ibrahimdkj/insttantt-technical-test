import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { errorText, hasErrorInput } from '../../utils/form-utils';

@Component({
  selector: 'app-input-element',
  templateUrl: './input-element.component.html',
  styleUrls: ['./input-element.component.scss'],
})
export class InputElementComponent implements OnInit {
  @Input() id?: string = '';
  @Input() label?: string;
  @Input() name?: string;
  @Input() type = 'text';
  @Input() placeholder = '';
  @Input() control: any;
  @Input() required = false;
  @Input() min?: string;
  @Input() max?: string;
  @Input() visible = true;
  @Input() disabled = false;

  errorText = errorText;
  hasErrorInput = hasErrorInput;

  constructor() {}

  ngOnInit(): void {}
}
