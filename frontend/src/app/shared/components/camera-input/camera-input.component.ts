import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { environment } from '../../../../environments/environment.prod';

@Component({
  selector: 'app-camera-input',
  templateUrl: './camera-input.component.html',
  styleUrls: ['./camera-input.component.scss'],
})
export class CameraInputComponent {
  @ViewChild('video') video?: ElementRef;
  @ViewChild('canvas') canvas?: ElementRef;
  showDialog = false;
  public image?: string;
  private stream?: MediaStream;
  rutaFoto?: string = '';

  @Input() id?: string = '';
  @Input() label?: string;
  @Input() name?: string;
  @Input() placeholder = '';
  @Input() control: any;
  @Input() required = false;
  @Input() min?: string;
  @Input() max?: string;
  @Input() visible = true;
  @Input() disabled = false;

  ngOnInit() {}

  setRutaFoto(foto: string) {
    if (foto) this.rutaFoto = environment.BASE_URL + '/uploads/' + foto;
  }

  openCamera() {
    this.startCamera();
  }

  stopCamera() {
    if (this.stream) {
      this.stream.getTracks().forEach((track) => track.stop()); // detener la cámara
      this.stream = undefined;
    }
    this.showDialog = false;
  }

  startCamera() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      this.showDialog = true;
      navigator.mediaDevices.getUserMedia({ video: true }).then((stream) => {
        if (this.video) {
          this.stream = stream;
          this.video.nativeElement.srcObject = this.stream;
          this.video.nativeElement.play();
        }
      });
    }
  }

  captureImage() {
    const context = this.canvas?.nativeElement.getContext('2d');
    if (this.canvas) {
      this.canvas.nativeElement.width = this.video?.nativeElement.videoWidth;
      this.canvas.nativeElement.height = this.video?.nativeElement.videoHeight;
    }
    context.drawImage(
      this.video?.nativeElement,
      0,
      0,
      this.video?.nativeElement.videoWidth,
      this.video?.nativeElement.videoHeight
    );
    this.image = this.canvas?.nativeElement.toDataURL('image/png');
    this.stopCamera();
  }
}
