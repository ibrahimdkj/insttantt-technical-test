import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CameraInputComponent } from './components/camera-input/camera-input.component';
import { ReactiveFormsModule } from '@angular/forms';

import { InputElementComponent } from './components/input-element/input-element.component';
import { SelectElementComponent } from './components/select-element/select-element.component';
import { FechaFormatoPipe } from './pipes/fecha-formato.pipe';

@NgModule({
  declarations: [
    CameraInputComponent,
    InputElementComponent,
    SelectElementComponent,
    FechaFormatoPipe,
  ],
  imports: [CommonModule, ReactiveFormsModule],
  exports: [
    CameraInputComponent,
    InputElementComponent,
    SelectElementComponent,
    FechaFormatoPipe
  ],
})
export class SharedModule {}
