import { NotificationType } from "../types/notification.type";

export interface NotificationInterface {
  title: string;
  icon: NotificationType;
  timer?: number;
}
