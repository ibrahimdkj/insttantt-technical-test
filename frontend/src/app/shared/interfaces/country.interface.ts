export interface CountryInterface {
  id: number;
  value: string;
  label: string;
  codigo?: string;
}
