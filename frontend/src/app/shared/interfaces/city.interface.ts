export interface CityInterface {
  id: number;
  id_country: number;
  value: string;
  label: string;
}
