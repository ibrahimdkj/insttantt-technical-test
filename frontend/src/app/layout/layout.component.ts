import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { appConfig } from '../core/config/app.config';
import { Layout, Schemes, Theme } from './interfaces/layout.types';

@Component({
  selector: 'layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  layout: Layout;
  scheme: Schemes;
  theme: Theme;

  /**
   * Constructor
   */
  constructor() {
    this.layout = appConfig.layout;
    this.scheme = appConfig.scheme;
    this.theme = appConfig.theme;
  }

  ngOnInit(): void {}
}
