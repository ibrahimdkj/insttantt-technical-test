export type Layout = 'default';
export type Schemes = 'light' | 'dark';
export type Theme = 'default' | string;
