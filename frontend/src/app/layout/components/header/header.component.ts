import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../../shared/services/notification.service';
import { AuthService } from '../../../core/auth/services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public isLoggedIn: boolean = false;
  public userSession: string = '';
  showMenu: boolean = false;

  constructor(
    private _notificacionService: NotificationService,
    private _authService: AuthService
  ) {}

  ngOnInit(): void {
    this._authService.isLoggedIn.subscribe((isLoggedIn: boolean) => {
      this.isLoggedIn = isLoggedIn;
    });
    this._authService.userSession.subscribe((userSession: string) => {
      this.userSession = userSession;
    });
  }

  cerrarSesion() {
    this._authService.logOut();
    this._notificacionService.showInfo('Vuelve pronto', 2000);
  }
  toogleMenu() {
    this.showMenu = !this.showMenu;
  }
}
