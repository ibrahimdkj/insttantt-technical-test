import { Component } from '@angular/core';
import { AuthService } from '../../../../core/auth/services/auth.service';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { agregarAniosFecha } from 'src/app/shared/utils/date-utils';
import ValidatorsPersonalizados from 'src/app/shared/validations/validations';
import { documentList } from 'src/app/shared/types/documents.type';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
})
export class SignUpComponent {
  formGroup: FormGroup;
  agregarAniosFecha = agregarAniosFecha;
  fechaActual = new Date();

  getDocumentTypes() {
    return documentList;
  }

  constructor(
    private readonly _authService: AuthService,
    private readonly _router: Router,
    private _formBuilderClass: FormBuilder
  ) {
    this.formGroup = this._formBuilderClass.group({
      email: [
        '',
        [Validators.required, Validators.email, Validators.minLength(6)],
      ],
      phoneNumber: ['', [Validators.required]],
      firstName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(100),
        ],
      ],
      lastName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(100),
        ],
      ],
      documentNumber: ['', [Validators.required]],
      documentType: ['', [Validators.required]],
      expeditionDate: [
        '',
        [
          Validators.required,
          ValidatorsPersonalizados.validacionMaxFecha(new Date()),
        ],
      ],
      birthdate: [
        '',
        [Validators.required, ValidatorsPersonalizados.validacionMayorDeEdad()],
      ],
    });
  }

  onSubmit() {
    if (this.formGroup?.invalid) {
      this.formGroup?.markAllAsTouched();
      return;
    }

    const body = {
      email: this.formGroup?.get('email')?.value,
      phoneNumber: this.formGroup?.get('phoneNumber')?.value,
      firstname: this.formGroup?.get('firstName')?.value,
      lastname: this.formGroup?.get('lastName')?.value,
      documentType: this.formGroup?.get('documentType')?.value,
      documentNumber: this.formGroup?.get('documentNumber')?.value,
      birthdate: this.formGroup?.get('birthdate')?.value,
      expeditionDate: this.formGroup?.get('expeditionDate')?.value,
    };

    console.log(body);
    this._authService.createUser(body);
  }
}
