import { Component } from '@angular/core';
import { AuthService } from '../../../../core/auth/services/auth.service';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
})
export class SignInComponent {
  formGroup: FormGroup;

  constructor(
    private readonly _authService: AuthService,
    private readonly _router: Router,
    private _formBuilderClass: FormBuilder
  ) {
    this.formGroup = this._formBuilderClass.group({
      username: [
        '',
        [Validators.required, Validators.email, Validators.minLength(6)],
      ],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });

    /* this.formGroup.controls['username'].setValue('ibrahimdkj');  */
  }

  onSubmit() {
    if (this.formGroup?.invalid) {
      this.formGroup?.markAllAsTouched();
      return;
    }
    const username = this.formGroup?.get('username')?.value;
    const password = this.formGroup?.get('password')?.value;
    this._authService.logIn(username, password);
  }
}
