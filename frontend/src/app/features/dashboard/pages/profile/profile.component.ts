import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../../../../core/auth/services/auth.service';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { agregarAniosFecha } from 'src/app/shared/utils/date-utils';
import ValidatorsPersonalizados from 'src/app/shared/validations/validations';
import { documentList } from 'src/app/shared/types/documents.type';
import { countrysList } from 'src/app/shared/types/countrys.type';
import { citesList } from 'src/app/shared/types/citys.type';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { UserService } from 'src/app/core/users/services/users.service';
import { CameraInputComponent } from 'src/app/shared/components/camera-input/camera-input.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {
  formGroup: FormGroup;
  agregarAniosFecha = agregarAniosFecha;
  fechaActual = new Date();
  @ViewChild('fotoPerfil') fotoPerfil?: CameraInputComponent;

  paisSeleccionado: string = '';
  private userUpdate: number;

  getDocumentTypes() {
    return documentList;
  }
  getCountrys() {
    return countrysList;
  }
  getCitys() {
    if (this.paisSeleccionado !== '')
      return citesList.filter(
        (e) => e.id_country.toString() === this.paisSeleccionado
      );
    return [];
  }

  selectPais(value: any) {
    this.paisSeleccionado = value;
  }

  constructor(
    private readonly _authService: AuthService,
    private readonly _userService: UserService,
    private readonly _router: Router,
    private _formBuilderClass: FormBuilder,
    private _notificationService: NotificationService
  ) {
    const dataSession = this._authService.getSessionData();
    this.userUpdate = dataSession?.id;
    this.formGroup = this._formBuilderClass.group({
      email: [
        '',
        [Validators.required, Validators.email, Validators.minLength(6)],
      ],
      phoneNumber: ['', [Validators.required]],
      firstName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(100),
        ],
      ],
      lastName: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(100),
        ],
      ],
      documentNumber: ['', [Validators.required]],
      documentType: ['', [Validators.required]],
      expeditionDate: [
        '',
        [
          Validators.required,
          ValidatorsPersonalizados.validacionMaxFecha(new Date()),
        ],
      ],
      birthdate: [
        '',
        [Validators.required, ValidatorsPersonalizados.validacionMayorDeEdad()],
      ],
      country: ['', []],
      city: ['', []],
      address: ['', [Validators.maxLength(100)]],
    });
  }

  ngOnInit() {
    this._userService.getUser(this.userUpdate).subscribe((response) => {
      console.log(response);
      this.formGroup.controls['email'].setValue(response?.email);
      this.formGroup.controls['phoneNumber'].setValue(response?.phoneNumber);
      this.formGroup.controls['firstName'].setValue(response?.firstname);
      this.formGroup.controls['lastName'].setValue(response?.lastname);
      this.formGroup.controls['documentType'].setValue(response?.documentType);
      this.formGroup.controls['documentNumber'].setValue(
        response?.documentNumber
      );
      this.formGroup.controls['birthdate'].setValue(
        response?.birthdate?.slice(0, 10)
      );
      this.formGroup.controls['expeditionDate'].setValue(
        response?.expeditionDate?.slice(0, 10)
      );
      this.formGroup.controls['country'].setValue(response?.country);
      this.formGroup.controls['city'].setValue(response?.city);
      this.formGroup.controls['address'].setValue(response?.address);
      if (response?.country&&response?.country!=='null'){
        this.selectPais(response?.country);

      } 
      this.fotoPerfil?.setRutaFoto(response?.photoProﬁle);

      /*    const fecha=new Date('2022-09-30T12:30:00.000Z');
      console.log(fecha.toISOString().slice(0, 10)); */
    });
  }
  convertirBase64ToBlob(base64: string): Blob {
    const partes = base64.split(',');
    const tipo = partes[0].split(':')[1];
    const bytes = atob(partes[1]);
    const array = new Uint8Array(bytes.length);
    for (let i = 0; i < bytes.length; i++) {
      array[i] = bytes.charCodeAt(i);
    }
    return new Blob([array], { type: tipo });
  }

  onSubmit() {
    if (this.formGroup?.invalid) {
      this.formGroup?.markAllAsTouched();
      return;
    }

    let formData = new FormData();
    formData.append('email', this.formGroup?.get('email')?.value);
    formData.append('phoneNumber', this.formGroup?.get('phoneNumber')?.value);
    formData.append('firstname', this.formGroup?.get('firstName')?.value);
    formData.append('lastname', this.formGroup?.get('lastName')?.value);
    formData.append('documentType', this.formGroup?.get('documentType')?.value);
    formData.append(
      'documentNumber',
      this.formGroup?.get('documentNumber')?.value
    );
    formData.append('birthdate', this.formGroup?.get('birthdate')?.value);
    formData.append(
      'expeditionDate',
      this.formGroup?.get('expeditionDate')?.value
    );
    formData.append('country', this.formGroup?.get('country')?.value);
    formData.append('city', this.formGroup?.get('city')?.value);
    formData.append('address', this.formGroup?.get('address')?.value);
    if (this.fotoPerfil?.image) {
      const blob = this.convertirBase64ToBlob(this.fotoPerfil?.image);
      formData.append('file', blob, 'foto-perfil.jpg');
    }

    this._authService
      .updateUserService(formData, this.userUpdate)
      .subscribe((response) => {
        this._notificationService.showSuccess(
          'Usuario actualizado éxitosamente'
        );
      });
  }
}
