import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { io, Socket } from 'socket.io-client';
import { AuthService } from 'src/app/core/auth/services/auth.service';

interface Hobby {
  name: string;
}
@Component({
  selector: 'app-hobbies',
  templateUrl: './hobbies.component.html',
  styleUrls: ['./hobbies.component.scss'],
})
export class HobbiesComponent implements OnInit {
  private socket: Socket;
  hobbies: Hobby[] = [];
  formGroup: FormGroup;
  private userUpdate: number;

  constructor(
    private _formBuilderClass: FormBuilder,
    private readonly _authService: AuthService
  ) {
    const dataSession = this._authService.getSessionData();
    this.userUpdate = dataSession?.id;

    this.formGroup = this._formBuilderClass.group({
      hobby: ['', [Validators.required, Validators.minLength(2)]],
    });
    this.socket = io('http://localhost:4000', {
      query: {
        userId: this.userUpdate,
      },
    });

    this.socket.on('init', (hobbies: Hobby[]) => {
      this.hobbies = hobbies;
    });

    this.socket.on('newHobby', (hobby: Hobby) => {
      this.hobbies.push(hobby);
    });
  }
  addHobby() {
    if (this.formGroup?.invalid) {
      this.formGroup?.markAllAsTouched();
      return;
    }

    this.socket.emit('addHobby', {
      name: this.formGroup?.get('hobby')?.value,
      id_usuario: this.userUpdate,
    });
    this.formGroup.reset();
  }

  ngOnInit(): void {}
}
